import java.util.Random;
import java.util.Scanner;

public class GuessNumber
{
	final static int MIN = 1;
	final static int MAX = 100;

	public static void main(String[] args)
	{
		Random random = new Random();
		int secret = random.nextInt(MIN, MAX + 1);
		
		int turn = 1;
		
		Scanner scanner = new Scanner(System.in);
		int guess;
		
		System.out.println("Try to guess my number! Is between " + MIN + " and " + MAX);
		
		do
		{
			System.out.println();
			System.out.print("Guess #" + turn + ": ");
			guess = scanner.nextInt();
			scanner.nextLine();

			if (secret == guess)
			{
				System.out.println("Yes! It took you " + turn + " turn(s) to guess my number.");
				break;
			}
			else if (secret < guess)
			{
				System.out.println("No! My number is smaller.");
			}
			else
			{
				System.out.println("No! My number is bigger.");
			}
			
			turn++;
		}
		while (true);
		
		scanner.close();
	}
}
